//-----------------------------------------------------------//
//-Projet Caméléon, 2020 : Coralie MARCHAU, Lucas MONTAGNIER-//

//-----------------------------------------------------------//
//-------------------------Imports--------------------------//
import java.util.*;

public class OrdiJ2 {

//----------------------------------------------------------//
//--------------------------Variables-----------------------//
	public int score;
	public String mode;//gb pour glonton brave, gt pour glouton téméraire et it pour intelligent téméraire

//---------------------------------------------------------//
//------------------------Constructeur---------------------//
	public OrdiJ2() {
		this.score=0;
	}

//---------------------------------------------------------//
//--------------------Fonctions publiques------------------//

	//choisi la prochaine case a joué en fonction de son mode et du plateau
	//Complexité : selon type de jeu O(1) glouton quelconque- O(log n) intelligent
	public Point choix_case(Plateau plateau){
		Point p;
		if(this.mode.equals("gb")){//glonton brave
			p = this.JouerGloutonBrave(plateau);
			System.out.println("Point trouvé par ordi gb: "+p.x+" ; "+p.y);
			return p;
		}
		else if(this.mode.equals("gt")){//glouton téméraire
			p = this.JouerGloutonTemeraire(plateau);
			System.out.println("Point trouvé par ordi gt: "+p.x+" ; "+p.y);
			return p;
		}
		else{//intelligent téméraire
			p = this.JouerIATemeraire(plateau);
			System.out.println("Point trouvé par ordi it: "+p.x+" ; "+p.y);
			return p;
		}
	}

	//-------------------------------------------------------------------------------
	//Choisi la case pour le glouton brave
	//compléxité: O(1)
	public Point JouerGloutonBrave(Plateau plateau){
		//Case c = plateau.quadtree.maxEvalScore('B',1);
		Case c = plateau.quadtree.getCaseMaxEvalScoreBraveBleu();//O(1)
		return new Point(c.P.x,c.P.y);
	}
	
	//-------------------------------------------------------------------------------
	//Choisi la case pour le glouton téméraire
	//compléxité: O(1)
	public Point JouerGloutonTemeraire(Plateau plateau){
		//Case c = plateau.quadtree.maxEvalScore('B',2);
		Case c = plateau.quadtree.getCaseMaxEvalScoreTemeraireBleu();//O(1)
		return new Point(c.P.x,c.P.y);
	}
	
	//-------------------------------------------------------------------------------
	//Choisi la case pour l'ordi intelligent téméraire
	//compléxité: O(log n)
	public Point JouerIATemeraire(Plateau plateau){

		//Case cTrouve = plateau.quadtree.maxEvalScore('B',2);//récupère le max
		Case cTrouve = plateau.quadtree.getCaseMaxEvalScoreTemeraireBleu();//O(1)
		
		if(plateau.quadtree.rechercheRegion(cTrouve.P.x,cTrouve.P.y,3).getNbCaseLibre()>=2){
			//si il reste plus que 2 cases libres il va jouer autre part
			cTrouve = trouve_case_region_case_libre_impair(plateau.quadtree);
		}
		
		if(cTrouve==null){//pas trouvé de region avec un nbCaseLibre impair
			cTrouve = trouve_case_region_libre(plateau.quadtree);
		}
		return new Point(cTrouve.P.x,cTrouve.P.y);
	}
	
	//-------------------------------------------------------------------------------
	//fonction récursive
	//trouve une region avec nbCaseLibre impair, retourne null si il n'y en a pas
	//compléxité: O(log n)
	private Case trouve_case_region_case_libre_impair(Region R){
		
		if(R instanceof PetiteRegion){
			if(R.getNbCaseLibre()%2==1){
				return R.maxEvalScore('B',2);//max d'une petite region en O(1)
			}
			else{
				return null;
			}
		}
		else{
			GrandeRegion GR = (GrandeRegion)R;
			Case cTrouve = null;
			if(GR.getTab()[0][0].getNbCaseLibre()>0){
				cTrouve = trouve_case_region_case_libre_impair(GR.getTab()[0][0]);
			}
			if(cTrouve==null && GR.getTab()[0][1].getNbCaseLibre()>0){
				cTrouve = trouve_case_region_case_libre_impair(GR.getTab()[0][1]);
			}
			if(cTrouve==null && GR.getTab()[1][0].getNbCaseLibre()>0){
				cTrouve = trouve_case_region_case_libre_impair(GR.getTab()[1][0]);
			}
			if(cTrouve==null && GR.getTab()[1][1].getNbCaseLibre()>0){
				cTrouve = trouve_case_region_case_libre_impair(GR.getTab()[1][1]);
			}
			return cTrouve;//retourne null si aucune des 4 sous-region n'est une region avec nbCaseLibre impair
		}
	}
	
	//-------------------------------------------------------------------------------
	//fonction récursive
	//trouve une region avec de la place
	//compléxité: O(log n)
	private Case trouve_case_region_libre(Region R){
	
		if(R instanceof PetiteRegion){
			if(R.getNbCaseLibre()>0){
				return R.maxEvalScore('B',2);//max d'une petite region en O(1)
			}
			else{
				return null;
			}
		}
		else{
			GrandeRegion GR = (GrandeRegion)R;
			Case cTrouve;
			cTrouve = trouve_case_region_libre(GR.getTab()[0][0]);
			if(cTrouve==null){
				cTrouve = trouve_case_region_libre(GR.getTab()[0][1]);
			}
			if(cTrouve==null){
				cTrouve = trouve_case_region_libre(GR.getTab()[1][0]);
			}
			if(cTrouve==null){
				cTrouve = trouve_case_region_libre(GR.getTab()[1][1]);
			}
			return cTrouve;//retourne null si aucune des 4 sous-region n'a de place libre ce qui est normalement impossible
		}
	}
	
}




