//-----------------------------------------------------------//
//-Projet Caméléon, 2020 : Coralie MARCHAU, Lucas MONTAGNIER-//

//-----------------------------------------------------------//
//-------------------------Imports--------------------------//
import java.util.*;

/*Représente : Une région de région soit un noeud du quadtree
Base de données présentes : tableau de région de 4 case*/
public class GrandeRegion implements Region{

//----------------------------------------------------------//
//--------------------------Variables-----------------------//
	public char couleur; //A = blanc, B = bleu, R = rouge
	public Region[][] tab=new Region[2][2]; //branches quadtree dans tableau 2x2

	//coordonnée du point de référencement de la région (haut à gauche)
	public int x;
	public int y;

	public int cote; //taille du côté de la région
	
	//Quantité de cases de chaque couleur
	public int nbCaseLibre;
	public int nbCaseRouge;
	public int nbCaseBleu;
	
	//Meilleure case à choisir pour l'ordinateur glouton
	public Case caseMaxEvalScoreBraveBleu;
	public Case caseMaxEvalScoreTemeraireBleu;


//---------------------------------------------------------//
//------------------------Constructeur---------------------//


//Constructeur noeud représentant la région de coordonnée x,y et côté t, vierge
//Complexité : O(n)
	public GrandeRegion(int t, int x, int y){
		this.nbCaseLibre=t*t;
		this.couleur='A';
		this.x=x;
		this.cote=t;
		this.y=y;
		if(cote==6){
			for(int i=0; i<2; i++){
				for(int j=0; j<2; j++){
				this.tab[i][j]=new PetiteRegion(x,y);
				}
			}
		}else{
			for(int i=0; i<2; i++){
				for(int j=0; j<2; j++){
				this.tab[i][j]=new GrandeRegion(t/2, x+i*(t/2), y+i*(t/2));
				}
			}
		}
		this.nbCaseRouge=0;
		this.nbCaseBleu=0;
	}

/* Constructeur pour noeud représentant la région
(de taille t et coordonnée x1, y1) dont les cases sont dans le tableau tabC */
//Complexité : O(nbCase)
	public GrandeRegion( Case[][] tabC, int t, int x1, int y1 ){
		this.x=x1;
		this.y=y1;
		this.cote=t;
		//initialisation des sous-tableaux
		Case[][] t1=new Case[t][t];
		Case[][] t2=new Case[t][t];
		Case[][] t3=new Case[t][t];
		Case[][] t4=new Case[t][t];

		//Division de tabC dans les sous-tableaux
		for(int i=0; i<t/2; i++){
			for(int j=0; j<t/2; j++){
				t1[i][j]=tabC[i][j];
				t3[i][j]=tabC[i+t/2][j];
				t2[i][j]=tabC[i][j+t/2];
				t4[i][j]=tabC[i+t/2][j+t/2];
			}
		}

		if(cote>6){ //Quand on est encore à la division en grande région
			this.tab[0][0]=new GrandeRegion(t1, t/2,x,y);
			this.tab[0][1]=new GrandeRegion(t2, t/2,x,y+t/2);
			this.tab[1][0]=new GrandeRegion(t3, t/2,x+t/2,y);
			this.tab[1][1]=new GrandeRegion(t4, t/2,x+t/2,y+t/2);
		}
		else { //Quand on arrive à la division en petite région
			this.tab[0][0]=new PetiteRegion(t1, x,y);
			this.tab[0][1]=new PetiteRegion(t2, x,y+t/2);
			this.tab[1][0]=new PetiteRegion(t3, x+t/2,y);
			this.tab[1][1]=new PetiteRegion(t4, x+t/2,y+t/2);
		}

		//évaluations du nombre de case libre
		this.nbCaseLibre=this.evalNbCaseLibre();

		//si il n'y a plus de cases libres, on évalue la couleur de région
		if(this.nbCaseLibre==0){ this.couleur=this.evalCouleurRegion(); }
		else{
			this.couleur='A'; //sinon elle est blanche
		}
		
		//initialisation des nbCases
		this.nbCaseRouge=this.tab[0][0].getNbCaseRouge()+this.tab[0][1].getNbCaseRouge()+this.tab[1][0].getNbCaseRouge()+this.tab[1][1].getNbCaseRouge();
		this.nbCaseBleu=this.tab[0][0].getNbCaseBleu()+this.tab[0][1].getNbCaseBleu()+this.tab[1][0].getNbCaseBleu()+this.tab[1][1].getNbCaseBleu();

	}
	
//---------------------------------------------------------//
//--------------------------Getters------------------------//
//Coût temporel des getters : O(1)
	public int getX(){return this.x;}
	public int getY(){return this.y;}
	public char getCouleur(){return this.couleur;}
	public Region[][] getTab(){return this.tab;}
	public int getCote(){return this.cote;}
	public int getNbCaseLibre(){return this.nbCaseLibre;}
	public int getNbCaseRouge(){return this.nbCaseRouge;}
	public int getNbCaseBleu(){return this.nbCaseBleu;}
	public Case getCaseMaxEvalScoreBraveBleu(){return this.caseMaxEvalScoreBraveBleu;}
	public Case getCaseMaxEvalScoreTemeraireBleu(){return this.caseMaxEvalScoreTemeraireBleu;}


//---------------------------------------------------------//
//--------------------Fonctions publiques------------------//

/*Action(s) : évalue la couleur de la région en regardant celle des sous-régions
Coût temporel : O(1)*/
	public char evalCouleurRegion(){
		char c=this.tab[0][0].couleur;
		char result=c;

		for(int i=0; i<2; i++){
			for(int j=0; j<2; j++){
				if(c!=tab[i][j].couleur){result='A';}
			}
		}
		return result;
	}

/*Préconditions : xC, yC doit être dans la région appelante et être une case blanche
Action(s) : Change la couleur de la case xC, yC en celle de c et vérifie la couleur des régions la contenant
Coût temporel : O(log(nbCase))) */
	public void changeCouleur(int xC, int yC, char c){

		//si xC, yC est dans la région en haut à gauche
		if(xC<this.cote/2+this.x&&yC<this.cote/2+this.y){
			this.tab[0][0].changeCouleur(xC,yC,c);
		}

		//si xC, yC est dans la région en haut à droite
		if(xC>=this.cote/2+this.x&&yC<this.cote/2+this.y){
			this.tab[1][0].changeCouleur(xC,yC,c);
		}

		//si xC, yC est dans la région en bas à gauche
		if(xC<this.cote/2+this.x&&yC>=this.cote/2+this.y){
			this.tab[0][1].changeCouleur(xC,yC,c);
		}

		//si xC, yC est dans la région en bas à droite
		if(xC>=this.cote/2+this.x&&yC>=this.cote/2+this.y){
			this.tab[1][1].changeCouleur(xC,yC,c);
		}

		//mise à jour du nb de case libre
		this.nbCaseLibre=this.nbCaseLibre-1;
		
		//augmente le nb de case de la couleur mise
		if(c=='R'){	this.nbCaseRouge=this.nbCaseRouge+1;}
		if(c=='B'){	this.nbCaseBleu=this.nbCaseBleu+1;}

		//si il n'y a plus de cases libres, on évalue la couleur de région
		if(this.nbCaseLibre==0){
			//on colorie la region en fonction du nomnre de sous-region deja colorié
			int[] nbRegionsRempli = this.nb_region_rempli();
			if(nbRegionsRempli[0]>2){
				this.remplirRegion('R');
			}
			else if(nbRegionsRempli[1]>2){
				this.remplirRegion('B');
			}
			else{//2 regions coloriées de chaque couleur
				this.remplirRegion(c);
			}
			this.couleur=this.evalCouleurRegion(); 
		}
		else{
			this.couleur='A'; //sinon elle est blanche
		}
		//actualisation en temps constant des nbCase rouge et bleu
		this.nbCaseRouge=this.tab[0][0].getNbCaseRouge()+this.tab[0][1].getNbCaseRouge()+this.tab[1][0].getNbCaseRouge()+this.tab[1][1].getNbCaseRouge();
		this.nbCaseBleu=this.tab[0][0].getNbCaseBleu()+this.tab[0][1].getNbCaseBleu()+this.tab[1][0].getNbCaseBleu()+this.tab[1][1].getNbCaseBleu();
		
	}

	/*Préconditions : xC, yC doit se trouver dans le tableau et t<=coté de la région appelante
	Action(s) : rend la région de taille t dans laquelle se trouve xC, yC
	Coût temporel : O(log(nbCase)) */
	public Region rechercheRegion(int xC, int yC, int t){
		//si on est de la bonne taille on suppose être au bon endroit
		if(this.cote==t){return this;}
		else{
			//Cas haut à gauche 
			if(xC<=(this.cote/2)+this.x&&yC<=(this.cote/2)+this.y){
				return this.tab[0][0].rechercheRegion(xC,yC,t);
			}
			//Cas haut à droite
			if(xC>(this.cote/2)+this.x&&yC<=(this.cote/2)+this.y){
				return this.tab[1][0].rechercheRegion(xC,yC,t);
			}
			//Cas bas à gauche
			if(xC<=(this.cote/2)+this.x&&yC>(this.cote/2)+this.y){
				return this.tab[0][1].rechercheRegion(xC,yC,t);
			}
			//Cas bas à droite
			return this.tab[1][1].rechercheRegion(xC,yC,t);
		}
	}

	/*Préconditions : xC, yC est dans une des sous-régions de l'appelant
	Action(s) : Renvoie la case de coordonnées xC, yC
	Coût temporel : O(log(nbCase))*/
	public Case rechercheCase(int xC, int yC){

		if(xC<=this.cote/2+this.x && yC<=this.cote/2+this.y){ return this.tab[0][0].rechercheCase(xC,yC); }
		if(xC>this.cote/2+this.x && yC<=this.cote/2+this.y){ return this.tab[1][0].rechercheCase(xC,yC); }
		if(xC<=this.cote/2+this.x && yC>this.cote/2+this.y){ return this.tab[0][1].rechercheCase(xC,yC); }
		return this.tab[1][1].rechercheCase(xC,yC);
	}

	/*Préconditions : xC, yC est dans une des sous-régions de l'appelant
	Action(s) : Renvoie la couleur de la case de coordonnées xC, yC
	Coût temporel : O(log(nbCase))*/
	public char rechercheCouleur(int xC, int yC){
		if(this.couleur!='A'){
			return this.couleur;
		}else{
			if(xC<=this.cote/2+this.x && yC<=this.cote/2+this.y){ return this.tab[0][0].rechercheCouleur(xC,yC); }
			if(xC>this.cote/2+this.x && yC<=this.cote/2+this.y){ return this.tab[1][0].rechercheCouleur(xC,yC); }
			if(xC<=this.cote/2+this.x && yC>this.cote/2+this.y){ return this.tab[0][1].rechercheCouleur(xC,yC); }
			return this.tab[1][1].rechercheCouleur(xC,yC);
		}
	}

	/*Préconditions : c = R ou B, choix = 1 ou 2
	Action(s) : Donne l'évaluation de score maximale de la région pour le joueur (R), 
	l'ordi (B) pour la variante de jeu Brave (1) ou Téméraire (2)
	Coût temporel : O(nbPetiteRegion) */
	public Case maxEvalScore(char c, int choix){

		Case result=tab[0][0].maxEvalScore(c,choix);
							
		for(int i=0; i<2; i++){
			for(int j=0; j<2; j++){
				if(c=='B'&&choix==1){ 
					if(result.evalScoreBraveBleu<tab[i][j].maxEvalScore(c,choix).evalScoreBraveBleu){
						result=tab[i][j].maxEvalScore(c,choix);
					}
				}
				if(c=='R'&&choix==1){	
					if(result.evalScoreBraveRouge<tab[i][j].maxEvalScore(c,choix).evalScoreBraveRouge){
						result=tab[i][j].maxEvalScore(c,choix);;
					}
				}
				if(c=='B'&&choix==2){
					if(result.evalScoreTemeraireBleu<tab[i][j].maxEvalScore(c,choix).evalScoreTemeraireBleu){
						result=tab[i][j].maxEvalScore(c,choix);;
					}
				}						if(c=='R'&&choix==2){
					if(result.evalScoreTemeraireRouge<tab[i][j].maxEvalScore(c,choix).evalScoreTemeraireRouge){
						result=tab[i][j].maxEvalScore(c,choix);;
					}
				}
			}				
		}
		return result;
	}

	/*Action(s) : Regarde récursivement le nombre de cases libres
	Coût temporel : O(nbCase)*/
	public int evalNbCaseLibre(){
		int result=0;
		for(int i=0;i<2;i++){
			for(int j=0;j<2;j++){
				result=result+this.tab[i][j].evalNbCaseLibre();
			}
		}
		return result;
	}
			
	/*Action(s) : Remplis toutes les cases de la région
	Coût temporel : O(log(n)) vu que normalement au moins 2 sous-régions sont de couleur c*/
	public void remplirRegion(char c){
	if(this.getCouleur()==c){}
	else{
		for(int i=0;i<2;i++){
			for(int j=0;j<2;j++){				
				tab[i][j].remplirRegion(c);
			}
		}
		
		this.couleur=c;
		this.nbCaseLibre=0;

		if(c=='R'){
			this.nbCaseRouge=this.cote*this.cote;
			this.nbCaseBleu=0;
		}
		if(c=='B'){
			this.nbCaseBleu=this.cote*this.cote;
			this.nbCaseRouge=0;
		}
	}
	}
	
	/*Action(s) : Retourne le nombre de sous-region remplie par R en [0] et par B en [1]
	Coût temporel : O(1) */
	public int[] nb_region_rempli(){
		char couleur;
		int nbR=0;
		int nbB=0;
		
		for(int i=0;i<2;i++){
			for(int j=0;j<2;j++){
				couleur=this.tab[i][j].getCouleur();	
				if(couleur!='A'){
					if(couleur=='R'){
						nbR++;
					}
					if(couleur=='B'){
						nbB++;
					}
				}
			}
		}
		int[] nb = {nbR,nbB};
		return nb;
	}
	
	/*Action(s) : fait +1 a this.nbCaseRouge et aux regions du point xC, yC
	Coût temporel : O(log(nbcase)) */
	public void add_nb_case_rouge(int xC, int yC){
		//Cas haut à gauche 
		if(xC<=(this.cote/2)+this.x && yC<=(this.cote/2)+this.y){
			this.tab[0][0].add_nb_case_rouge(xC, yC);
		}
		//Cas haut à droite
		if(xC>(this.cote/2)+this.x && yC<=(this.cote/2)+this.y){
			this.tab[1][0].add_nb_case_rouge(xC, yC);
		}
		//Cas bas à gauche
		if(xC<=(this.cote/2)+this.x && yC>(this.cote/2)+this.y){
			this.tab[0][1].add_nb_case_rouge(xC, yC);
		}
		//Cas bas à droite
		if(xC>(this.cote/2)+this.x && yC>(this.cote/2)+this.y){
			this.tab[1][1].add_nb_case_rouge(xC, yC);
		}
	}
	
	/*Action(s) : fait +1 a this.nbCaseBleu  et aux regions du point xC, yC
	Coût temporel : O(log(nbCase)) */
	public void add_nb_case_bleu(int xC, int yC){
		//Cas haut à gauche 
		if(xC<=(this.cote/2)+this.x && yC<=(this.cote/2)+this.y){
			this.tab[0][0].add_nb_case_bleu(xC, yC);
		}
		//Cas haut à droite
		if(xC>(this.cote/2)+this.x && yC<=(this.cote/2)+this.y){
			this.tab[1][0].add_nb_case_bleu(xC, yC);
		}
		//Cas bas à gauche
		if(xC<=(this.cote/2)+this.x && yC>(this.cote/2)+this.y){
			this.tab[0][1].add_nb_case_bleu(xC, yC);
		}
		//Cas bas à droite
		if(xC>(this.cote/2)+this.x && yC>(this.cote/2)+this.y){
			this.tab[1][1].add_nb_case_bleu(xC, yC);
		}
	}
	
	/*Action(s) : recalcule les this.caseMaxEvalScore Bleu
	Coût temporel : O(log(nbCase) */
	public void maj_case_max(int xC, int yC){
		//Cas haut à gauche 
		if(xC<=(this.cote/2)+this.x && yC<=(this.cote/2)+this.y){
			this.tab[0][0].maj_case_max(xC, yC);
		}
		//Cas haut à droite
		if(xC>(this.cote/2)+this.x && yC<=(this.cote/2)+this.y){
			this.tab[1][0].maj_case_max(xC, yC);
		}
		//Cas bas à gauche
		if(xC<=(this.cote/2)+this.x && yC>(this.cote/2)+this.y){
			this.tab[0][1].maj_case_max(xC, yC);
		}
		//Cas bas à droite
		if(xC>(this.cote/2)+this.x && yC>(this.cote/2)+this.y){
			this.tab[1][1].maj_case_max(xC, yC);
		}
		//maj de this.caseMaxEvalScore
		int maxBra=0;
		int maxTem=0;
		int xBra=0;
		int yBra=0;
		int xTem=0;
		int yTem=0;
		for(int i=0;i<2;i++){
			for(int j=0;j<2;j++){
				if(this.tab[i][j].getCaseMaxEvalScoreBraveBleu().evalScoreBraveBleu > maxBra && this.tab[i][j].getCouleur()=='A'){
					maxBra = this.tab[i][j].getCaseMaxEvalScoreBraveBleu().evalScoreBraveBleu;
					xBra=i;
					yBra=j;
				}
				if(this.tab[i][j].getCaseMaxEvalScoreTemeraireBleu().evalScoreTemeraireBleu > maxTem && this.tab[i][j].getCouleur()=='A'){
					maxTem = this.tab[i][j].getCaseMaxEvalScoreTemeraireBleu().evalScoreTemeraireBleu;
					xTem=i;
					yTem=j;
				}
			}
		}
		this.caseMaxEvalScoreBraveBleu = this.tab[xBra][yBra].getCaseMaxEvalScoreBraveBleu();
		this.caseMaxEvalScoreTemeraireBleu = this.tab[xTem][yTem].getCaseMaxEvalScoreTemeraireBleu();
		System.out.println("MAX GRANDE: "+this.x+" ; "+this.y+"  maxbrave: "+this.caseMaxEvalScoreBraveBleu.P.x+";"+this.caseMaxEvalScoreBraveBleu.P.y+"  maxTem: "+this.caseMaxEvalScoreTemeraireBleu.P.x+";"+this.caseMaxEvalScoreTemeraireBleu.P.y);
	}
	
	
	/*Action(s) : initialisation des this.caseMaxEvalScore Bleu
	Coût temporel : O(nbCase)*/
	public void init_case_max(){
		for(int i=0;i<2;i++){
			for(int j=0;j<2;j++){
				this.tab[i][j].init_case_max();
			}
		}
		//maj de this.caseMaxEvalScore
		int maxBra=0;
		int maxTem=0;
		int xBra=0;
		int yBra=0;
		int xTem=0;
		int yTem=0;
		for(int i=0;i<2;i++){
			for(int j=0;j<2;j++){
				if(this.tab[i][j].getCaseMaxEvalScoreBraveBleu().evalScoreBraveBleu > maxBra  && this.tab[i][j].getCouleur()=='A'){
					maxBra = this.tab[i][j].getCaseMaxEvalScoreBraveBleu().evalScoreBraveBleu;
					xBra=i;
					yBra=j;
				}
				if(this.tab[i][j].getCaseMaxEvalScoreTemeraireBleu().evalScoreTemeraireBleu > maxTem  && this.tab[i][j].getCouleur()=='A'){
					maxTem = this.tab[i][j].getCaseMaxEvalScoreTemeraireBleu().evalScoreTemeraireBleu;
					xTem=i;
					yTem=j;
				}
			}
		}
		//System.out.println("MAX BRA: "+maxBra+"  MAX TEM: "+maxTem);
		this.caseMaxEvalScoreBraveBleu = this.tab[xBra][yBra].getCaseMaxEvalScoreBraveBleu();
		this.caseMaxEvalScoreTemeraireBleu = this.tab[xTem][yTem].getCaseMaxEvalScoreTemeraireBleu();
		//System.out.println("GRANDE FINISH: "+this.x+" ; "+this.y+"  maxbrave: "+this.caseMaxEvalScoreBraveBleu.P.x+";"+this.caseMaxEvalScoreBraveBleu.P.y+"  maxTem: "+this.caseMaxEvalScoreTemeraireBleu.P.x+";"+this.caseMaxEvalScoreTemeraireBleu.P.y);
	}
}

