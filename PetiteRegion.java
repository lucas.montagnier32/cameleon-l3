//-----------------------------------------------------------//
//-Projet Caméléon, 2020 : Coralie MARCHAU, Lucas MONTAGNIER-//

//-----------------------------------------------------------//
//-------------------------Imports--------------------------//
import java.util.*;


/*Représente : Une petite région soit une feuille du quadtree
Base de données présentes : tableau de case de 9 cases*/
public class PetiteRegion implements Region{

//----------------------------------------------------------//
//--------------------------Variables-----------------------//
	public Case[][] tab=new Case[3][3]; //Cases en bout d'arbre
	public char couleur; //A = blanc, B = bleu, R = rouge

	//coordonnée du point de référencement de la région (haut à gauche)
	public int x;
	public int y;

	public int cote; //taille du côté de la région
	
	//Quantité de cases de chaque couleur
	public int nbCaseLibre;
	public int nbCaseRouge;
	public int nbCaseBleu;
	
	//Meilleure case à choisir pour l'ordinateur glouton
	public Case caseMaxEvalScoreBraveBleu;
	public Case caseMaxEvalScoreTemeraireBleu;


//---------------------------------------------------------//
//------------------------Constructeur---------------------//

//Constructeur feuille représentant la région de coordonnée x,y de côté 3, vierge
//Complexité : O(1)
	public PetiteRegion(int x, int y){
		this.x=x;
		this.y=y;
		this.nbCaseLibre=9;
		for(int i=0; i<3; i++){
			for(int j=0; j<3; j++){
			this.tab[i][j]=new Case(x+i,y+j);
			}
		}
		this.couleur='A';
		this.nbCaseRouge=0;
		this.nbCaseBleu=0;

	}

	/* Constructeur pour feuille représentant la région
	(de côté 3 et coordonnée x1, y1) dont les cases sont dans le tableau tabC */
	//Complexité : O(1)
	public PetiteRegion(Case[][] tabC,int x1,int y1){
		this.x=x1;
		this.y=y1;
		this.nbCaseLibre=0;
		this.nbCaseRouge=0;
		this.nbCaseBleu=0;
		for(int i=0; i<3; i++){
			for(int j=0; j<3; j++){
			this.tab[i][j]=tabC[i][j];
				if(tab[i][j].couleur=='A'){this.nbCaseLibre=this.nbCaseLibre+1;}
				if(tab[i][j].couleur=='R'){this.nbCaseRouge=this.nbCaseRouge+1;}
				if(tab[i][j].couleur=='B'){this.nbCaseBleu=this.nbCaseBleu+1;}
			}
		}
		this.couleur=evalCouleurRegion();
	}

//---------------------------------------------------------//
//--------------------------Getters------------------------//
	public int getX(){return this.x;}
	public int getY(){return this.y;}
	public char getCouleur(){return this.couleur;}
	public Case[][] getTab(){return this.tab;}
	public int getCote(){return this.cote;}
	public int getNbCaseLibre(){return this.nbCaseLibre;}
	public int getNbCaseRouge(){return this.nbCaseRouge;}
	public int getNbCaseBleu(){return this.nbCaseBleu;}
	public Case getCaseMaxEvalScoreBraveBleu(){return this.caseMaxEvalScoreBraveBleu;}
	public Case getCaseMaxEvalScoreTemeraireBleu(){return this.caseMaxEvalScoreTemeraireBleu;}

//---------------------------------------------------------//
//--------------------Fonctions publiques------------------//

/*Action(s) : évalue la couleur de la région en regardant celle des sous-régions
Coût temporel : O(1)*/
	public char evalCouleurRegion(){
		char c=this.tab[0][0].couleur;
		char result=c;

		for(int i=0; i<3; i++){
			for(int j=0; j<3; j++){
				if(c!=tab[i][j].couleur){result='A';}
			}
		}

		return result;
	}

	/*Préconditions : xC, yC doit être dans la région appelante et être une case blanche
	Action(s) : Change la couleur de la case xC, yC en celle de c et vérifie la couleur de sa région
	Coût temporel : O(1) */
	public void changeCouleur(int xC, int yC, char c){
		tab[xC-this.x][yC-this.y].couleur=c;
		this.couleur=evalCouleurRegion();
		this.nbCaseLibre=this.nbCaseLibre-1;

		//augmente le nb de case de la couleur mise
		if(c=='R'){	this.nbCaseRouge=this.nbCaseRouge+1;	}
		if(c=='B'){	this.nbCaseBleu=this.nbCaseBleu+1;	}

		if(this.nbCaseLibre==0){
			this.remplirRegion(c);
			this.couleur=this.evalCouleurRegion();
		}
	}

	/*Préconditions : xC, yC doit se trouver dans le tableau et t<=coté de la région appelante
	Action(s) : rend la région, il n'existe pas de région plus petite
	Coût temporel : O(1) */
	public Region rechercheRegion(int xC, int yC, int t){
		return this;
	}

	/*Préconditions : xC, yC est dans la région de l'appelant
	Action(s) : Renvoie la case de coordonnées xC, yC
	Coût temporel : O(1)*/
	public Case rechercheCase(int xC, int yC){
		int i=0;
		int j=0;
		while(xC!=this.x+i||yC!=this.y+j){
			if(xC>this.x+i){i=i+1;}
			if(yC>this.y+j){j=j+1;}
		}
		return tab[i-1][j-1];
	}

	/*Préconditions : xC, yC est dans la région de l'appelant
	Action(s) : Renvoie la couleur de la case de coordonnées xC, yC
	Coût temporel : O(1)*/
	public char rechercheCouleur(int xC, int yC){
		if(this.couleur!='A'){
			return this.couleur;
		}
			else{
				int i=0;
				int j=0;
				while(xC!=x+i||yC!=y+j){
					if(xC>this.x+i){i=i+1;}
					if(yC>this.y+j){j=j+1;}
				}
				return tab[i][j].couleur;
		}
	}

	/*Action(s) : Regarde le nombre de cases libres
	Coût temporel : O(1)*/
	public int evalNbCaseLibre(){
		return nbCaseLibre;
	}

	/*Préconditions : c = R ou B, choix = 1 ou 2
	Action(s) : Donne l'évaluation de score maximale de la région pour le joueur (R), l'ordi (B) pour la variante de jeu Brave (1) ou Téméraire (2)
	Coût temporel : O(1) */
	public Case maxEvalScore(char c, int choix){
		Case result=tab[0][0];
		for(int i=0; i<3; i++){
			for(int j=0; j<3; j++){
				if(c=='B'&&choix==1){
					if(result.evalScoreBraveBleu<tab[i][j].evalScoreBraveBleu){
						result=tab[i][j];
					}
				}
				if(c=='R'&&choix==1){
					if(result.evalScoreBraveRouge<tab[i][j].evalScoreBraveRouge){
						result=tab[i][j];
					}
				}
				if(c=='B'&&choix==2){
					if(result.evalScoreTemeraireBleu<tab[i][j].evalScoreTemeraireBleu){
						result=tab[i][j];
					}
				}
				if(c=='R'&&choix==2){
					if(result.evalScoreTemeraireRouge<tab[i][j].evalScoreTemeraireRouge){
						result=tab[i][j];
					}
				}
			}
		}
		return result;
	}

	/*Action(s) : Remplis toutes les cases de la région
	Coût temporel : O(1) */
	public void remplirRegion(char c){
		for(int i=0; i<3; i++){
			for(int j=0; j<3; j++){
				tab[i][j].couleur=c;

			}
		}
		this.couleur=c;
		this.nbCaseLibre=0;

		if(c=='R'){
			this.nbCaseRouge=9;
			this.nbCaseBleu=0;
		}
		if(c=='B'){
			this.nbCaseBleu=9;
			this.nbCaseRouge=0;
		}
	}

	/*Action(s) : fait +1 a this.nbCaseRouge
	Coût temporel : O(1) */
	public void add_nb_case_rouge(int xC, int yC){
		if(this.nbCaseRouge<9){
			this.nbCaseRouge+=1;
		}
		if(this.nbCaseBleu>0){
			this.nbCaseBleu-=1;
		}
	}

	/*Action(s) : fait +1 a this.nbCaseBleu
	Coût temporel : O(1) */
	public void add_nb_case_bleu(int xC, int yC){
		if(this.nbCaseBleu<9){
			this.nbCaseBleu+=1;
		}
		if(this.nbCaseRouge>0){
			this.nbCaseRouge-=1;
		}
	}

	/*Action(s) : recalcule les this.caseMaxEvalScore Bleu
	Coût temporel : O(1) */
	public void maj_case_max(int xC, int yC){
		int maxBra=0;
		int maxTem=0;
		int xBra=0;
		int yBra=0;
		int xTem=0;
		int yTem=0;
		for(int i=0; i<3; i++){
			for(int j=0; j<3; j++){
				if(this.tab[i][j].evalScoreBraveBleu > maxBra && this.tab[i][j].couleur=='A'){
					maxBra = this.tab[i][j].evalScoreBraveBleu;
					xBra=i;
					yBra=j;
				}
				if(this.tab[i][j].evalScoreTemeraireBleu > maxTem && this.tab[i][j].couleur=='A'){
					maxTem = this.tab[i][j].evalScoreTemeraireBleu;
					xTem=i;
					yTem=j;
				}
			}
		}
		this.caseMaxEvalScoreBraveBleu = this.tab[xBra][yBra];
		this.caseMaxEvalScoreTemeraireBleu = this.tab[xTem][yTem];
	}

	/*Action(s) : initialisation des this.caseMaxEvalScore Bleu
	Coût temporel : O(1)*/
	public void init_case_max(){
		this.maj_case_max(1,1);

	}

}
