//-----------------------------------------------------------//
//-Projet Caméléon, 2020 : Coralie MARCHAU, Lucas MONTAGNIER-//

//-----------------------------------------------------------//
//-------------------------Imports--------------------------//
import java.util.*;

/*Représentation du Quadtree, GrandeRegion en étant les noeuds et PetiteRegion
en étant les feuiles*/
public interface Region{

//----------------------------------------------------------//
//--------------------------Variables-----------------------//
	public char couleur='A'; //A = blanc, B = bleu, R = rouge

	//coordonnée du point de référencement de la région (haut à gauche)
	public int x=0;
	public int y=0;

	public int cote=3; //taille du côté de la région
	
	//Quantité de cases de chaque couleur
	public int nbCaseLibre=0;
	public int nbCaseRouge=0;
	public int nbCaseBleu=0;
	
	//pour permettre aux ordis de jouer plus rapidement
	public Case caseMaxEvalScoreBraveBleu=null;
	public Case caseMaxEvalScoreTemeraireBleu=null;

//---------------------------------------------------------//
//--------------------------Getters------------------------//
	//Coût temporel des getters : O(1)
	public int getX();	
	public int getY();	
	public char getCouleur();	
	public int getCote();		
	public int getNbCaseLibre();	
	public int getNbCaseRouge();	
	public int getNbCaseBleu();	
	public Case getCaseMaxEvalScoreBraveBleu();	
	public Case getCaseMaxEvalScoreTemeraireBleu();	

//---------------------------------------------------------//
//--------------------Fonctions publiques------------------//

/*Préconditions : xC, yC doit être dans la région appelante et être une case blanche
Action(s) : Change la couleur de la case xC, yC en celle de c et vérifie la couleur des régions la contenant
Coût temporel : O(log n) */
	public void changeCouleur(int xC, int yC, char c);

	/*Action(s) : évalue la couleur de la région en regardant celle des sous-régions
	Coût temporel : O(1)*/
	public char evalCouleurRegion();

	/*Préconditions : xC, yC est dans une des sous-régions de l'appelant
	Action(s) : Renvoie la case de coordonnées xC, yC
	Coût temporel : O(log n)*/
	public Case rechercheCase(int xC, int yC);

	/*Préconditions : xC, yC doit se trouver dans le tableau et
										t<=coté de la région appelante
	Action(s) :	rend la région de taille t dans laquelle se trouve xC, yC
	Coût temporel : O(log n) */
	public Region rechercheRegion(int xC, int yC, int t);

	/*Préconditions : xC, yC est dans une des sous-régions de l'appelant
	Action(s) : Renvoie la couleur de la case de coordonnées xC, yC
	Coût temporel : O(log n)*/
	public char rechercheCouleur(int xC, int yC);

	/*Action(s) : Regarde récursivement le nombre de cases libres
	Coût temporel : O(n)*/
	public int evalNbCaseLibre();

	/*Préconditions : c = R ou B, choix = 1 ou 2
	Action(s) : Donne l'évaluation de score maximale de la région pour le
							joueur (R), l'ordi (B) pour la variante de jeu Brave (1) ou
							Téméraire (2)
	Coût temporel : O(n) */
	public Case maxEvalScore(char c, int choix);
	
	/*Action(s) : Remplis toutes les cases de la région
	Coût temporel : O(n) */
	public void remplirRegion(char c);
	
	/*Action(s) : fait +1 a this.nbCaseRouge
	Coût temporel : O(1) */
	public void add_nb_case_rouge(int xC, int yC);
	
	/*Action(s) : fait +1 a this.nbCaseBleu
	Coût temporel : O(1) */
	public void add_nb_case_bleu(int xC, int yC);
	
	/*Action(s) : recalcule les this.caseMaxEvalScore Bleu
	Coût temporel : O(log(nbCase) */
	public void maj_case_max(int xC, int yC);
	
	/*Action(s) : initialisation des this.caseMaxEvalScore Bleu
	Coût temporel : O(nbCase)*/
	public void init_case_max();

}
