//-----------------------------------------------------------//
//-Projet Caméléon, 2020 : Coralie MARCHAU, Lucas MONTAGNIER-//
import java.util.*;

public class Case{

//----------------------------------------------------------//
//--------------------------Variables-----------------------//
	public Point P;//coordonnées de la case
	public char couleur;//couleur de la case
	//les evalScore donne le nombre de point qu'un joueur peut gagner ne jouant cette case selon le version du jeu
	public int evalScoreBraveRouge;
	public int evalScoreBraveBleu;
	public int evalScoreTemeraireRouge;
	public int evalScoreTemeraireBleu;
	

//---------------------------------------------------------//
//------------------------Constructeur---------------------//	

	//créé une case blanche
	//compléxité: O(1)
	public Case(int x, int y){
		this.P = new Point(x,y);
		this.couleur = 'A';
		this.evalScoreBraveRouge = 0;
		this.evalScoreBraveBleu = 0;
		this.evalScoreTemeraireRouge = 0;
		this.evalScoreTemeraireBleu = 0;
	}
	
//---------------------------------------------------------//
//--------------------Fonctions publiques------------------//
	//met a zero tout les evalSCore de la case
	//compléxité: O(1)
	public void mettre_a_zero(){
		this.evalScoreBraveRouge = 0;
		this.evalScoreBraveBleu = 0;
		this.evalScoreTemeraireRouge = 0;
		this.evalScoreTemeraireBleu = 0;
	}
	
}
