//-----------------------------------------------------------//
//-Projet Caméléon, 2020 : Coralie MARCHAU, Lucas MONTAGNIER-//

//-----------------------------------------------------------//
//-------------------------Imports--------------------------//
import java.util.*;

public class Joueur1 {
//----------------------------------------------------------//
//--------------------------Variables-----------------------//
	public int score;
	Scanner in;
	
//---------------------------------------------------------//
//------------------------Constructeur---------------------//
	//Complexité : O(1)	
	public Joueur1() {
		this.score=0;
		this.in = new Scanner(System.in);
	}
	
//---------------------------------------------------------//
//--------------------Fonctions publiques------------------//	
	//Action(s) : Permet de choisir une case
	//Complexité : O(1)
	public Point choix_case() {
		System.out.println("Saisir le numéro de colonne:");
		int x = this.in.nextInt();
		System.out.println("Saisir le numéro de ligne:");
		int y = this.in.nextInt();
		return new Point(x,y);
	}
}
