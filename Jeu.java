//-----------------------------------------------------------//
//-Projet Caméléon, 2020 : Coralie MARCHAU, Lucas MONTAGNIER-//

//-----------------------------------------------------------//
//-------------------------Imports--------------------------//
import java.util.*;

public class Jeu {

//----------------------------------------------------------//
//--------------------------Variables-----------------------//
	public Plateau plateau;
	public Joueur1 J1;
	public OrdiJ2 J2;
	public Scanner in;
	public char mode;//b si brave, t si téméraire
	public Point maxEvalScore;//maxEvalScore pour ordi glouton brave uniquement
	public boolean plateauSaisie;

//---------------------------------------------------------//
//--------------------Fonctions publiques------------------//

	//permet de créé un jeu (lance une partie)
	//compléxité: O(1)
	public Jeu() {
		this.in = new Scanner(System.in);
		J1 = new Joueur1();
		J2 = new OrdiJ2();
		choix_plateau();
		choix_mode();
		if(this.mode=='t'){//si téméraire alors choix du mode de l'ordi
			choix_ordi();
			this.initialisation_evalScores_temeraire();
		}
		else{
			this.J2.mode="gb";
		}
		//initialisation des this.caseMaxEvalScore Bleu dans le quadtree
		this.plateau.quadtree.init_case_max();
		System.out.println("mode de l'ordi: "+this.J2.mode);
		this.in.nextLine();//vide la ligne
		lancer_jeu();
	}
	
	//-------------------------------------------------------------------------------
	//lance la partie, s'arrete quand il n'y a plus de place sur le plateau
	//Complexité : O(n*n)
	public void lancer_jeu(){
		this.plateau.afficher_plateau();
		while(this.plateau.placeRestante>0){
			System.out.println("\nplace restante: "+this.plateau.placeRestante);
			tour_de_J1();
			this.plateau.afficher_plateau();
			
			if(this.plateau.placeRestante>0){
				System.out.println("Saisissez n'importe quoi pour faire jouer l'ordi:");
				this.in.nextLine();
				tour_de_J2();
				this.plateau.afficher_plateau();
			}
		}
		//fin du jeu
		CalculScore();
		if(this.J1.score>this.J2.score){
			System.out.println("Vous avez gagné :-)");
		}
		else if(this.J1.score<this.J2.score){
			System.out.println("Vous avez perdu :-(");
		}
		else{
			System.out.println("Égalité :-|");
		}
	}
	
	//-------------------------------------------------------------------------------
	//permet a J1 de choisir parmis plusiseurs options pendant son tour
	//Complexité : O(n)
	public void tour_de_J1(){
		Point P;
		//les options de J1
		System.out.println("-----Votre tour de jouer-----");
		System.out.println("-choix d'une case pour evaluation du score en tapant \"eval\"");
		System.out.println("-choix d'une case pour jouer en tapant \"joue\"");
		System.out.println("-tapez autre chose pour avoir les scores actuels");
		
		String choix = this.in.nextLine();
		
		while(!choix.equalsIgnoreCase("joue")){
		
			if(choix.equalsIgnoreCase("eval")){//donne l'évaluation d'une case
				P = this.J1.choix_case();
				while(!this.plateau.est_libre(P.x,P.y)){//peut pas faire une eval sur une case occupée
					System.out.println("! Case déjà occupée !");
					P = this.J1.choix_case();
				}
				
				if(this.mode!='t'){
					this.EvalCaseBrave(this.plateau.plateau[P.x-1][P.y-1]);
				}
				else{
					this.EvalCaseTemeraire(this.plateau.plateau[P.x-1][P.y-1]);
				}
				
			}
			else {//affichage des scores
				CalculScore();
			}
			System.out.println("saisir (eval, joue ou score):");
			choix = this.in.nextLine();
			
		}
		
		//le joueur joue en choissisant une case
		P = this.J1.choix_case();
		while(!this.plateau.est_libre(P.x,P.y)){//redemande une case si la case est occupée
			System.out.println("! Case déjà occupée !");
			P = this.J1.choix_case();
		}
		//modification du plateau en fonction du mode de jeu
		int[] scores = this.plateau.modif_scores_et_plateau(new Point(P.x-1,P.y-1), 'R', this.mode);
		this.J1.score+=scores[0];
		this.J2.score+=scores[1];
		if(this.mode=='t'){
			this.J1.score=scores[0];
			this.J2.score=scores[1];
		}
		this.plateau.placeRestante--;

	}
	
	//-------------------------------------------------------------------------------
	//l'ordi J2 joue
	//compléxité: O(1) pour les ordis gloutons et O(nbCase) pour l'IA
	public void tour_de_J2(){
		
		//choix du case par l'ordi en fonction de son mode déja paramétré
		Point Pmax = this.J2.choix_case(this.plateau);

		//modification du plateau
		int[] scores = this.plateau.modif_scores_et_plateau(new Point(Pmax.x-1,Pmax.y-1), 'B', this.mode);
		this.J1.score+=scores[0];
		this.J2.score+=scores[1];
		if(this.mode=='t'){
			this.J1.score=scores[0];
			this.J2.score=scores[1];
		}
		this.plateau.placeRestante--;
	}
	
	//-------------------------------------------------------------------------------
	//créé le plateau vide ou a remplir en fontion du choix du joueur
	//compléxité: O(n)
	public void choix_plateau(){
		System.out.println("Plateau vide (0) ou plateau a saisir (1) ?");
		int choixPlateau = this.in.nextInt();
		if(choixPlateau==1) {
			this.plateau = new Plateau();
			this.plateau.RemplirPlateau();
			//initialisation des différents varibles selon le mode de jeu
			this.initialisation_variables_quand_plateau_saisie();
			this.plateauSaisie=true;
		}
		else {//créé un plateau vide de taille 3*2^k
			System.out.println("Saisir k tel que taille=3*2^k:");
			int k = in.nextInt();
			this.plateau = new Plateau(k);
			this.plateauSaisie=false;
		}
	}
	
	//-------------------------------------------------------------------------------
	//choix du mode de jeu
	//compléxité: O(1)
	public void choix_mode(){
		System.out.println("Version Brave (b) ou version Téméraire (t) ?");
		String aux = this.in.next();
		this.mode = aux.charAt(0);
		if(this.mode != 'b' && this.mode != 't'){
			this.mode = 'b';//version brave par défaut
		}
	}
	
	//-------------------------------------------------------------------------------
	//choix du mode de l'ordi si mode téméraire choisi
	//compléxité: O(1)
	public void choix_ordi(){
		System.out.println("Ordi Glouton (0) ou intelligent (1) ?");
		int choixOrdi = this.in.nextInt();
		if(choixOrdi==1){
			this.J2.mode="it";
		}
		else{
			this.J2.mode="gt";
		}
	}
	
	//-------------------------------------------------------------------------------
	//initialise les evalScores des cases, les scores des joueurs et le nombre de place restante dans le plateau quand celui ci est saisie
	//compléxité: O(nbCase) mais 1 seul fois au début du jeu
	public void initialisation_variables_quand_plateau_saisie(){
		this.plateau.placeRestante = 0;
		this.J1.score = 0;
		this.J2.score = 0;
		char couleurCase;
		
		for(int i=0;i<this.plateau.taille;i++){
			for(int j=0;j<this.plateau.taille;j++){
				couleurCase = this.plateau.plateau[i][j].couleur;
				if(couleurCase =='A'){
					this.plateau.placeRestante++;
				}
				if(couleurCase =='R'){
					this.J1.score++;
				}
				if(couleurCase =='B'){
					this.J2.score++;
				}
				//initialisation des evalScores des cases blanches
				if(couleurCase=='A'){
					initialisation_evalScores(this.plateau.plateau[i][j]);
				}	
			}
		}
	}
	
	//-------------------------------------------------------------------------------
	//initialise les evalScores des cases blanches
	//compléxité: O(log(n))
	private void initialisation_evalScores(Case centre){
		//couleur de centre == 'A'
		ArrayList<Case> voisins = this.plateau.voisinage(centre);
		char couleurVoisin;
		
		for(Case c : voisins){
			couleurVoisin = c.couleur;
			if(couleurVoisin!='A'){
				if(couleurVoisin=='R'){
					centre.evalScoreBraveBleu++;
					if(this.plateau.quadtree.rechercheRegion(c.P.x,c.P.y,3).getCouleur()=='A'){
						centre.evalScoreTemeraireBleu++;//augmente le scoreTemeraire voisins dans region non acquise
					}
				}
				if(couleurVoisin=='B'){
					centre.evalScoreBraveRouge++;
					if(this.plateau.quadtree.rechercheRegion(c.P.x,c.P.y,3).getCouleur()=='A'){
						centre.evalScoreTemeraireRouge++;//augmente le scoreTemeraire voisins dans region non acquise
					}
				}
			}
		}
	}
	
	//-------------------------------------------------------------------------------
	//va augmenter les evalScoreTemeraire des cases seul dans une region quand un plateau est saisie
	//compléxité: O(log(n))
	public void initialisation_evalScores_temeraire(){
		this.plateau.modifEvalScoreTemeraire(this.plateau.quadtree,0,0);
	}
	
	//-------------------------------------------------------------------------------
	//calcule affiche le score du plateau actuel
	//compléxité: O(1)
	public void CalculScore(){
		System.out.println("Score de J1(rouge): "+this.J1.score+" ; Score de J2(bleu): "+this.J2.score);
	}
	
	//-------------------------------------------------------------------------------
	//affiche le score que chaque joueur peut atteindre en brave, s’il choisit la case passée en paramètre.
	//compléxité: O(1)
	public void EvalCaseBrave(Case C){
		//on fait le +1 car les attribut evalScore ne prennent pas en compte la case choisi par le joueur
		System.out.println("Score avec cette case en brave pour vous: "+(C.evalScoreBraveRouge+1));
		System.out.println("Score avec cette case en brave pour J2: "+(C.evalScoreBraveBleu+1));
	}
	
	//-------------------------------------------------------------------------------
	//affiche le score que chaque joueur peut atteindre en téméraire, s’il choisit la case passée en paramètre.
	//compléxité: O(1)
	public void EvalCaseTemeraire(Case C){
		//on fait le +1 car les attribut evalScore ne prennent pas en compte la case choisi par le joueur
		System.out.println("Score avec cette case en téméraire pour vous: "+(C.evalScoreTemeraireRouge+1));
		System.out.println("Score avec cette case en téméraire pour J2: "+(C.evalScoreTemeraireBleu+1));
	}
	
}




