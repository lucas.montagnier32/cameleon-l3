//-----------------------------------------------------------//
//-Projet Caméléon, 2020 : Coralie MARCHAU, Lucas MONTAGNIER-//

/*Représente : Un point dans l'espace du plateau grâce à ses coordonnées*/
public class Point {

//----------------------------------------------------------//
//--------------------------Variables-----------------------//
	public int x;
	public int y;

//---------------------------------------------------------//
//------------------------Constructeur---------------------//	
	//Constructeur point
	//Complexité : O(1)
	public Point(int x,int y) {
		this.x=x;
		this.y=y;
	}
}
