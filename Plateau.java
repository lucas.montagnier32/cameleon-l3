//-----------------------------------------------------------//
//-Projet Caméléon, 2020 : Coralie MARCHAU, Lucas MONTAGNIER-//

//-----------------------------------------------------------//
//-------------------------Imports--------------------------//
import java.util.*;
import java.io.File;
import java.io.FileNotFoundException;

/*Représentation du plateau de jeu*/
public class Plateau {

//----------------------------------------------------------//
//--------------------------Variables-----------------------//
	public Case[][] plateau;
	public int taille;//longueur d'un coté
	public int placeRestante;
	public Region quadtree;

//---------------------------------------------------------//
//------------------------Constructeur---------------------//
	//créé un plateau vide
	//compléxité: O(nbCase)
	public Plateau(int k) {
		this.taille = 3*(int)Math.pow(2,k);
		this.plateau = new Case[this.taille][this.taille];

		for(int i=0;i<this.taille;i++) {
			for(int j=0;j<this.taille;j++) {
				this.plateau[i][j]=new Case(i+1,j+1);
				this.plateau[i][j].couleur = 'A';
			}
		}
		this.placeRestante=this.taille*this.taille;
		//création quadtree
		if(taille>3){
			this.quadtree=new GrandeRegion(this.plateau,taille,0,0);
		}
		else{
			this.quadtree=new PetiteRegion(this.plateau,0,0);
		}
	}

	//-------------------------------------------------------------------------------
	//constructeur vide
	//créé un plateau vide O(1)
	public Plateau() {
	}

//---------------------------------------------------------//
//--------------------Fonctions publiques------------------//

	//permet de passer un plateau a partir d'un fichier
	//compléxité: O(nbcase)
	public void RemplirPlateau() {
		Scanner in = new Scanner(System.in);
		//saisie du nom du fichier
		System.out.println("Saisir fichier:");
		String fichier = in.nextLine();
		try{
			in = new Scanner(new File(fichier));
		}
		catch(FileNotFoundException e){
			e.printStackTrace();
		}

		System.out.println("Saisir taille:");
		this.taille = in.nextInt();
		this.plateau = new Case[this.taille][this.taille];

		System.out.println("Saisir plateau:");
		String ligne="";
		String split;
		in.nextLine();
		for(int i=0;i<this.taille;i++) {
			ligne=in.nextLine();//saisie d'une ligne
			for(int j=0;j<this.taille;j++) {
				split=ligne.substring(j,j+1);//coupe la ligne en caractère
				this.plateau[j][i]=new Case(j+1,i+1);
				this.plateau[j][i].couleur=split.charAt(0);//met chaque caractère dans le tableau
			}
		}
		in.close();
		//création du quadtree
		if(taille>3){
			this.quadtree=new GrandeRegion(this.plateau,taille,0,0);
		}else{
			this.quadtree=new PetiteRegion(this.plateau,0,0);
		}
	}

	//-------------------------------------------------------------------------------
	//affiche le plateau
	//compléxité: O(nbCase)
	public void afficher_plateau() {
		System.out.println();
		System.out.println("PLATEAU:");
		for(int i=0;i<this.taille;i++) {
			if(i!=0 && i%3==0){
				System.out.println();
			}
			for(int j=0;j<this.taille;j++) {
				if(j!=0 && j%3==0){
					System.out.print("|");
				}
				if(this.plateau[j][i].couleur=='R'){//en rouge
					System.out.print("\u001B[31m"+this.plateau[j][i].couleur+"\u001B[0m");
				}
				else if(this.plateau[j][i].couleur=='B'){//en bleu
					System.out.print("\u001B[34m"+this.plateau[j][i].couleur+"\u001B[0m");
				}
				else{//en blanc
					System.out.print(this.plateau[j][i].couleur);
				}
			}
			System.out.println();
		}
	}

	//-------------------------------------------------------------------------------
	//vérifie que la case x,y est libre
	//compléxité: O(1)
	public boolean est_libre(int x,int y) {
		return this.plateau[x-1][y-1].couleur=='A';
	}

	//-------------------------------------------------------------------------------
	//retourne le voisinage de la case passé en paramètre
	//compléxité: O(1)
	public ArrayList<Case> voisinage(Case c){
		ArrayList<Case> voisins = new ArrayList<Case>();
		Point p = c.P;

		//parcours des 9 cases
		for(int i=p.y-1;i<p.y+2;i++){
			for(int j=p.x-1;j<p.x+2;j++){
				if(i>0 && j>0 && i<=this.taille && j<=this.taille){//si le point i,j est bien dans le plateau
					voisins.add(plateau[j-1][i-1]);
				}
			}
		}
		return voisins;
	}


	//-------------------------------------------------------------------------------
	//modifie le plateau et les scores braves en fonction de la case p.x,p.y et de la couleur passé en paramètre
	//compléxité: O(nbCase)
	public int[] modif_scores_et_plateau(Point P, char color, char mode){
		int scoreRouge=0;
		int scoreBleu=0;

		Case centre = this.plateau[P.x][P.y];
		centre.couleur = color;//coloriage du centre
		centre.mettre_a_zero();//on met les evalScore a zero de cet case


		if(color=='R'){
			scoreRouge++;
		}
		if(color=='B'){
			scoreBleu++;
		}

		modifEvalScore(centre);//O(log(nbCase))

		//modification des couleurs des voisins
		ArrayList<Case> voisins = this.voisinage(centre);
		for(Case c : voisins){
			if(c.couleur!='A' && c.couleur!=color){
				if(mode!='t'){//version brave
					if(color=='R'){
						c.couleur='R';
						scoreRouge++;
						scoreBleu--;
					}
					if(color=='B'){
						c.couleur='B';
						scoreBleu++;
						scoreRouge--;
					}
				}
				else{//version téméraire
					//colorie les voisins que si region non acquise
					Region R = this.quadtree.rechercheRegion(c.P.x,c.P.y,3);//O(log(nbCase))
					//System.out.println("CASE: "+c.P.x+" ; "+c.P.y);
					//System.out.println("REGIONTROUVE: "+R.getX()+" ; "+R.getY());
					if(R.getCouleur()=='A'){
						if(color=='R'){
							c.couleur='R';
							this.quadtree.add_nb_case_rouge(c.P.x,c.P.y);//O(log(nbCase))
						}
						if(color=='B'){
							c.couleur='B';
							this.quadtree.add_nb_case_bleu(c.P.x,c.P.y);//O(log(nbCase))
						}
					}
				}
			}
		}

		if(mode=='t'){//mode teméraire
			//change la couleur de la case x,y et modifie les regions si besoin
			this.quadtree.changeCouleur(P.x,P.y,color);//O(log(nbCase))

			//on doit modfier quelques evalScores des cases autour de la region si rempli
			int taille = this.quadtree.getCote();
			Region R = this.quadtree.rechercheRegion(P.x,P.y,taille/2);//O(log(nbCase))
			while(R.getNbCaseLibre()!=0 && taille>2){//on cherche la plus grosses region qui a été remplie (log*log)
				R = this.quadtree.rechercheRegion(P.x,P.y,taille);//O(log(nbCase))
				taille = taille/2;
			}
			if(R.getNbCaseLibre()==0){//une region a été remplie
				//modifie les evalScoresTemeraire sur les cases voisines de la region R
				this.modif_eval_score_tem_region_rempli(R);//O(taille(R))
			}

			//System.out.println("rouge: "+this.quadtree.getNbCaseRouge()+" ; bleu: "+this.quadtree.getNbCaseBleu());
			//modifie le scores en temps constant
			scoreRouge=this.quadtree.getNbCaseRouge();
			scoreBleu=this.quadtree.getNbCaseBleu();
			//modifie les evalScore des dernières cases de region
			this.modifEvalScoreTemeraire(this.quadtree,0,0);//O(log(nbCase))
			//recalcule les caseMaxEvalScore Bleu
			//this.maj_max_voisins_petite_region(this.plateau[P.x][P.y]);//O(log(nbCase))
		}
		//recalcule les caseMaxEvalScore Bleu
		this.maj_max_voisins_petite_region(this.plateau[P.x][P.y]);//O(log(nbCase))

		int[] scores = {scoreRouge,scoreBleu};
		return scores;
	}

	//-------------------------------------------------------------------------------
	//modifie les attibuts evalScore rouge et bleu des case voisines et plus que voisines si l'on choisi la case passé en paramètre
	//la case c passé en paramètre doit déja être colorié
	//FONCTION A METTRE AVANT DE CHANGER LES COULEURS DES VOISINS SINON ÇA MARCHE PAS
	//compléxité: O(rechercheRegion()) = O(log(nbCase))
	public void modifEvalScore(Case centre){
		ArrayList<Case> voisins = this.voisinage(centre);
		char couleurCentre = centre.couleur;
		char couleur;

		for(Case c : voisins){
			couleur = c.couleur;
			if(couleur=='A'){//si case blanche alors changer evalScore de cette case en fonction de la couleur du centre
				if(couleurCentre=='B'){
					c.evalScoreBraveRouge++;
					if(this.quadtree.rechercheRegion(c.P.x,c.P.y,3).getCouleur()=='A'){
						c.evalScoreTemeraireRouge++;//augmente le evalscoreTemeraire voisins dans region non acquise
					}
				}
				if(couleurCentre=='R'){
					c.evalScoreBraveBleu++;
					if(this.quadtree.rechercheRegion(c.P.x,c.P.y,3).getCouleur()=='A'){
						c.evalScoreTemeraireBleu++;//augmente le evalscoreTemeraire voisins dans region non acquise
					}
				}
			}
			else{//si coloré alors changer les evalScore des voisins de cette case
				if(couleurCentre=='B' && couleur=='R'){
					modifEvalScoreVoisinsBlanc(c);
				}
				if(couleurCentre=='R' && couleur=='B'){
					modifEvalScoreVoisinsBlanc(c);
				}
			}
		}
	}

	//-------------------------------------------------------------------------------
	//utilisé dans la fonction modifEvalScore()
	//modifie les evalScore des cases blanches autour de la case passé en paramètre centre
	//compléxité: O(1)
	public void modifEvalScoreVoisinsBlanc(Case centre){
		ArrayList<Case> voisins = this.voisinage(centre);
		char couleurCentre = centre.couleur;
		char couleur;

		//la case centre va changer de couleur après la fonction ce qui implique que si sa couleur est bleu (deviendra rouge après)
		//alors l'evalScoreRouge des cases blanches voisines vont diminuer et evalScoreBleu va augmenter
		for(Case c : voisins){
			couleur = c.couleur;
			if(couleur=='A'){
				if(couleurCentre=='B'){
					c.evalScoreBraveRouge--;
					c.evalScoreBraveBleu++;
					c.evalScoreTemeraireRouge--;
					c.evalScoreTemeraireBleu++;
				}
				if(couleurCentre=='R'){
					c.evalScoreBraveBleu--;
					c.evalScoreBraveRouge++;
					c.evalScoreTemeraireBleu--;
					c.evalScoreTemeraireRouge++;
				}
			}
		}
	}

	//-------------------------------------------------------------------------------
	//ajoute aux evalScoreTemeraire de la region passé en paramètre si une case est dernière de sa region
	//compléxité: O(log(nbCase))
	public void modifEvalScoreTemeraire(Region R, int nbPetiteRegionsRouge, int nbPetiteRegionsBleu){

		if(R instanceof PetiteRegion){
			if(R.getNbCaseLibre()==1){
				//on cherche la dernière case de la region
				PetiteRegion pr = (PetiteRegion)R;
				for(int i=0;i<3;i++){
					for(int j=0;j<3;j++){
						if(pr.getTab()[i][j].couleur=='A'){
							if(nbPetiteRegionsBleu!=3){
								pr.getTab()[i][j].evalScoreTemeraireRouge=(9*(nbPetiteRegionsBleu+1)-1);
							}
							else{
								pr.getTab()[i][j].evalScoreTemeraireRouge=-(9*(nbPetiteRegionsRouge+1)+1);
							}
							if(nbPetiteRegionsRouge!=3){
								pr.getTab()[i][j].evalScoreTemeraireBleu=(9*(nbPetiteRegionsRouge+1)-1);
							}
							else{
								pr.getTab()[i][j].evalScoreTemeraireBleu=-(9*(nbPetiteRegionsBleu+1)+1);
							}
						}
					}
				}
			}
		}
		else{
			GrandeRegion gr = (GrandeRegion)R;
			int[] nbRegionRempli = gr.nb_region_rempli();
			//on va voir dans la region ou ya une seul case libre
			if(gr.getTab()[0][0].getNbCaseLibre()==1){
				modifEvalScoreTemeraire(gr.tab[0][0], nbRegionRempli[0], nbRegionRempli[1]);
			}
			if(gr.getTab()[0][1].getNbCaseLibre()==1){
				modifEvalScoreTemeraire(gr.tab[0][1], nbRegionRempli[0], nbRegionRempli[1]);
			}
			if(gr.getTab()[1][0].getNbCaseLibre()==1){
				modifEvalScoreTemeraire(gr.tab[1][0], nbRegionRempli[0], nbRegionRempli[1]);
			}
			if(gr.getTab()[1][1].getNbCaseLibre()==1){
				modifEvalScoreTemeraire(gr.tab[1][1], nbRegionRempli[0], nbRegionRempli[1]);
			}
		}
	}

	//-------------------------------------------------------------------------------
	//retourne 9 case au max étant dans les petites regions voisines de la case passé en paramètre
	//compléxité: O(log(nbCase))
	public ArrayList<Case> voisins_petite_region(Case c){
		ArrayList<Case> voisins = new ArrayList<Case>();
		Point p = c.P;

		for(int i=p.y-3;i<=p.y+3;i+=3){
			for(int j=p.x-3;j<=p.x+3;j+=3){
				if(i>0 && j>0 && i<=this.taille && j<=this.taille){//si la petite region existe
					voisins.add(this.quadtree.rechercheCase(j,i));//O(log(nbCase))
				}
			}
		}
		return voisins;
	}

	//-------------------------------------------------------------------------------
	//recalcule les caseMaxEvalScore Bleu dans les 9 petites regions au max autour de la case joué
	//compléxité: O(log(nbCase))
	public void maj_max_voisins_petite_region(Case c){
		ArrayList<Case> voisins = voisins_petite_region(c);//O(log(nbCase))

		for(Case cas : voisins){
			this.quadtree.maj_case_max(cas.P.x,cas.P.y);//O(log(nbCase))
		}
	}


	//-------------------------------------------------------------------------------
	//retourne les cases voisines d'une region
	//compléxité: O(taille(R))
	public ArrayList<Case> cases_voisins_region(Region R){
		ArrayList<Case> voisins = new ArrayList<Case>();
		int x0 = R.getX();
		int y0 = R.getY();
		int cote = R.getCote();

		for(int i=x0-1;i<=cote;i++){//voisins du dessus
			if(i>=0 && y0-1>=0 && i<this.taille && this.plateau[i][y0-1].couleur=='A'){
				voisins.add(this.plateau[i][y0-1]);
			}
		}
		for(int i=x0-1;i<=cote;i++){//voisins du dessous
			if(i>=0 && y0+cote<this.taille && i<this.taille && this.plateau[i][y0+cote].couleur=='A'){
				voisins.add(this.plateau[i][y0+cote]);
			}
		}
		for(int j=y0;j<cote;j++){//voisins de gauche
			if(x0-1>=0 && this.plateau[x0-1][j].couleur=='A'){
				voisins.add(this.plateau[x0-1][j]);
			}
		}
		for(int j=y0;j<cote;j++){//voisins du droite
			if(x0+cote<this.taille && this.plateau[x0+cote][j].couleur=='A'){
				voisins.add(this.plateau[x0+cote][j]);
			}
		}

		return voisins;
	}

	//-------------------------------------------------------------------------------
	//modifie les evalScoresTemeraire sur les cases voisines de la region R
	//compléxité: O(log(n))
	public void modif_eval_score_tem_region_rempli(Region R){
		ArrayList<Case> voisins = cases_voisins_region(R);

		for(Case cas : voisins){
			cas.evalScoreTemeraireRouge = this.eval_case_tem(cas)[0];
			cas.evalScoreTemeraireBleu = this.eval_case_tem(cas)[1];
		}
	}

	//-------------------------------------------------------------------------------
	//donne l'eval score d'une case C en brave
	//compléxité: O(1)
	public int[] eval_case_tem(Case C){
		ArrayList<Case> voisins = this.voisinage(C);
		int evals[] = {0,0};
		if(C.couleur!='A'){
			return evals;
		}
		for(Case cas : voisins){
			if(this.quadtree.rechercheRegion(cas.P.x,cas.P.y,3).getCouleur()=='A'){
				if(cas.couleur=='R'){
					evals[0]++;
				}
				if(cas.couleur=='B'){
					evals[1]++;
				}
			}
		}
		return evals;
	}

}
